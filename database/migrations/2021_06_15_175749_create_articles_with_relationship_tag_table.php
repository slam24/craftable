<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesWithRelationshipTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles_with_relationship_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('articles_with_relationship_id');
            $table->foreign('articles_with_relationship_id', 'awr_id_foreign')
                ->references('id')
                ->on('articles_with_relationships')
                ->onDelete('cascade');

            $table->unsignedInteger('tag_id');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles_with_relationship_tag');
    }
}
