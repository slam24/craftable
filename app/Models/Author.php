<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'id',
        'title',
        'created_at',
        'updated_at'
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/authors/'.$this->getKey());
    }

    public function articlesWithRelationships()
    {
        return $this->hasMany(ArticlesWithRelationship::class);
    }
}
