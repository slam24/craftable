<?php

namespace App\Http\Controllers;
use App\Models\Tag;
use App\Models\ArticlesWithRelationship;
use App\Http\Resources\TagCollection;
use App\Http\Resources\TagResource;
use App\Http\Resources\ArticlesWithRelationshipResource;
use App\Http\Resources\ArticlesWithRelationshipCollection;

use Illuminate\Http\Request;

class ResourcesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if ($request->input('Option') == 'getTags') {
            $tags = new TagCollection(Tag::paginate(10));

            return response()->json(['tags' => $tags], 200);
        }else if($request->input('Option') == 'getArticles'){
            $articles = new ArticlesWithRelationshipCollection(ArticlesWithRelationship::paginate(10));

            return response()->json(['articles' => $articles], 200);
        }else{
            return response()->json(['error' => 'Not Found'], 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if ($request->input('Option') == 'getTags') {
            $tags = new TagResource(Tag::find($id));

            foreach ($tags as $key => $value) {
                $value['insertData'] = "data";
            }

            return response()->json(['tags' => $tags], 200);
        }else if($request->input('Option') == 'getArticles'){
            $articles = new ArticlesWithRelationshipResource(ArticlesWithRelationship::find($id));

            return response()->json(['articles' => $articles], 200);
        }else{
            return response()->json(['error' => 'Not Found'], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
