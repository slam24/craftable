<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AuthorResource;

class ArticlesWithRelationshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author_id' => $this->author_id,
            'author' => $this->author,
            'tags' => $this->tags,
            'gallery' => $this->getMedia('gallery'),
            'cover' => $this->getMedia('cover'),
            'pdf' => $this->getMedia('pdf'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
