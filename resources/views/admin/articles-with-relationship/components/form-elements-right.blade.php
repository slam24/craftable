<div class="card">
    <div class="card-header">
        <i class="fa fa-check"></i>{{ trans('brackets/admin-ui::admin.forms.publish') }}
    </div>
    <div class="card-block">

        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('published_at'), 'has-success': fields.published_at && fields.published_at.valid }">
            <label for="published_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-2' : 'col-md-4'">{{ trans('admin.articles-with-relationship.columns.published_at') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <div class="input-group input-group--custom">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <datetime v-model="form.published_at" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('published_at'), 'form-control-success': fields.published_at && fields.published_at.valid}" id="published_at" name="published_at" placeholder="Select date and time"></datetime>
                </div>
                <div v-if="errors.has('published_at')" class="form-control-feedback form-text" v-cloak>@{{errors.first('published_at') }}</div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <i class="fa fa-user"></i>{{ trans('admin.articles-with-relationship.columns.author_id') }}
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-sm-auto form-group">
                <p>{{ __('Select author/s') }}</p>
            </div>
            <div class="col col-lg-12 col-xl-12 form-group">
                <multiselect
                     v-model="form.author"
                     :options="{{ $authors->map(function($author) { return ['id' => $author->id, 'title' =>  $author->title]; })->toJson() }}"
                     label="title"
                     track-by="id"
                     :multiple="false"
                     placeholder="{{ __('Type to search a author/s') }}">
                </multiselect>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
       <span><i class="fa fa-tags"></i> Tags </span>
    </div>

    <div class="card-block">
        <div class="form-group row align-items-center"
             :class="{'has-danger': errors.has('tags'), 'has-success': this.fields.tags && this.fields.tags.valid }">
            <label for="author_id"
                   class="col-form-label text-center col-md-4 col-lg-3">Tags</label>
            <div class="col-md-8 col-lg-9">

                <multiselect
                        v-model="form.tags"
                        :options="{{ $tags->map(function($tag) { return ['id' => $tag->id, 'name' =>  $tag->name]; })->toJson() }}"
                        :multiple="true"
                        track-by="id"
                        label="name"
                        tag-placeholder="{{ __('Select Tags') }}"
                        placeholder="{{ __('Tags') }}">
                </multiselect>

                <div v-if="errors.has('tags')" class="form-control-feedback form-text" v-cloak>@{{
                    errors.first('tags') }}
                </div>
            </div>
        </div>

    </div>
</div>

<div class="card">
    @if($mode === 'edit')
        @include('brackets/admin-ui::admin.includes.media-uploader', [
            'mediaCollection' => $articlesWithRelationship->getMediaCollection('cover'),
            'media' => $articlesWithRelationship->getThumbs200ForCollection('cover'),
            'label' => 'Cover photo'
        ])
    @else
        @include('brackets/admin-ui::admin.includes.media-uploader', [
            'mediaCollection' => app(App\Models\ArticlesWithRelationship::class)->getMediaCollection('cover'),
            'label' => 'Cover photo'
        ])
    @endif
</div>

